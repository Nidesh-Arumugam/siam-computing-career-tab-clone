import React from 'react';
import '../App.css';
import footerImag from '../assets/footer-logo.png';
import { FaFacebookF, FaInstagram, FaTwitter, FaLinkedinIn, FaYoutube, FaGooglePlusG } from "react-icons/fa";


function Footer() {

  const footerNavlist = ["service","About","Resources"]

  const navlist = [
    {
      id: 1,
      title: "Service",
      child: true,
      children: [
        {
          id: 1.1,
          title: "Product strategy and consulting",
        },
        {
          id: 1.2,
          title: "MVP Development",
        },
        {
          id: 1.2,
          title: "Dedicated Product Team",
        },
      ],
    },
    {
      id: 3,
      title: "About",
      child: true,
      children: [
        {
          id: 3.1,
          title: "Our Journey",
        },
        {
          id: 3.2,
          title: "Life at  Siam",
        },
        {
          id: 3.3,
          title: "Careers",
        },
      ],
    },
    {
      id: 4,
      title: "Resoures",
      child: true,
      children: [
        {
          id: 4.1,
          title: "Blogs",
        },
        {
          id: 4.2,
          title: "Case Studies",
        },
        {
          id: 4.3,
          title: "Guides",
        },
      ],
    },
  ];

  return (
    <div className="footer">
      <div className='footerBody'>
      <div className='footerCompanyInfo mb-5'>
          <img src={footerImag} alt="footerLogo" className='footerLogo'/>
          <p className='mb-0 companyEmail cursorPointer f18'> hello@siamcomputing.com </p>
          <p className='f18'> No 30, 1st Main Road,<br/>Shastri Nagar,<br/> Adyar, Chennai,<br/>Tamil Nadu 600020 </p>
         
      </div>

      <div className='footerNavHead'>
          {navlist.map((item,index) =>
          <div className='overallFooter'>
           <h6 className='footerNavlist f14'>
           {item.title}
          </h6>

          <div>
          {item.children.map((subItem,index)=> 
          <div className='footerSubnavlist cursorPointer f14'>
          {subItem.title}
          </div>
          )}
          </div>
          
          </div>
          )}
      </div>
      

      </div>
      
       <div className='socialmediaIcons d-flex justify-content-between pt-4'>
              <FaFacebookF size={19} className=""/>
              <FaInstagram size={19}/>
              <FaTwitter size={19}/>
              <FaLinkedinIn size={19}/>
              <FaYoutube size={19}/>
              <FaGooglePlusG size={20}/>
          </div>


      <div className='horizontalLine'>
      <hr className='text-white m-0 '></hr>
      <p className='mb-3 text-center pt-4'> © Copyright 2022 siamcomputing. All Rights Reserved. </p>
      </div>
      
    </div>
  );
}

export default Footer;
